const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLSchema,
    GraphQLList,
    GraphQLNonNull,
    GraphQLBoolean,
    GraphQLScalarType,
} = require('graphql')

const books = [
    {
        id: 0,
        title : 'mobyDick',
        available : true,
    }
]

const BookType = new GraphQLObjectType({
    name: 'Book',
    fields: {
        id: {type : GraphQLInt },
        title: { type : GraphQLString },
        available: { type: GraphQLBoolean }
    }
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        books: {
            type: new GraphQLList(BookType),
            resolve(parentValues, args) {
                return books
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
})