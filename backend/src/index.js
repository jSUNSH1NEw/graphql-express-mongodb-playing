import express from "express"
import { graphqlHTTP } from "express-graphql"
import { ApolloServer, gql } from "apollo-server-express"
import mongoose from "mongoose"

// * import for Server 
import schema from "./setup/shema"
import { resolvers } from "./resolver"
import { typeDefs } from "./typeDefs"


const server = async () => {
    const app = express()
    const PORT = 1337

    const server = new ApolloServer({
        typeDefs,
        resolvers
    })

    server.applyMiddleware({app})

    try {
        await mongoose.connect("mongodb+srv://sunshine:12345@cluster0.zu8cb.mongodb.net/Cluster0?retryWrites=true&w=majority",
         { useUnifiedTopology: true } && { useNewUrlParser: true } )
    }
    catch(err){
        console.log(err +'There is a problem with mongoose connect')
    }
    

    app.get('/', (req, res) => res.send('hello boy ?'))


/    app.use('/iql', graphqlHTTP({
        graphiql: true,
        schema: schema
    }))

    app.listen(PORT, () => {
        console.log("Listening on post :" + PORT)
    }) 
    
}
 server()

