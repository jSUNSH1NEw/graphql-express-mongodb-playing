import { gql } from 'apollo-server-express'

export const typeDefs = gql`

type Query {
    helloWorld: String!
    dogs: [Dogs!]!
}

type Dogs {
    id: ID!
    name: String!
}

type Mutation {
    createDogs(name: String!): Dogs!
}

`