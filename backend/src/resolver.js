import { Dogs } from "./models/AnimaleTest/doggs"


export const resolvers = {
     Query : {
         helloWorld: () => "Hi There Welcome on 1337",
         dogs: () => Dogs.find()
     },

     Mutation: {
         createDogs: async(_, { name }) => {
             const PUPPY = new Dogs({ name })
             await PUPPY.save()
             return PUPPY
         }
     }

 }